# go-localised

[![go reference](https://pkg.go.dev/static/frontend/badge/badge.svg)](https://pkg.go.dev/gitlab.com/cptpackrat/go-localised)
[![pipeline status](https://gitlab.com/cptpackrat/go-localised/badges/main/pipeline.svg)](https://gitlab.com/cptpackrat/go-localised/-/commits/main)
[![coverage report](https://gitlab.com/cptpackrat/go-localised/badges/main/coverage.svg)](https://gitlab.com/cptpackrat/go-localised/-/commits/main)

A simple utility for storing and accessing a piece of data with one or more translations.

## Usage
```go
package main

import (
  "fmt"

  "gitlab.com/cptpackrat/go-localised"
)

var title = localised.
  New("en", "Release Notes").
  Add("es", "Notas de lanzamiento")

var notes = localised.
  New("en", []string{
    "Fixed a bug.",
    "Fixed another bug.",
    "Added a feature.",
  }).
  Add("es", []string{
    "Se corrigió un error.",
    "Se corrigió otro error.",
    "Se agregó una función.",
  })

func main() {
  fmt.Printf("%s:\n", title.MustGet())
  for _, note := range notes.MustGet() {
    fmt.Printf(" - %s\n", note)
  }
}
```

```
$ go build -o app
$ LANG=en ./app
Release Notes:
 - Fixed a bug.
 - Fixed another bug.
 - Added a feature.
$ LANG=es ./app
Notas de lanzamiento:
 - Se corrigió un error.
 - Se corrigió otro error.
 - Se agregó una función.
```

Supports marshalling to/from JSON. For the following collection of strings:
```go
localised.
  New("en", "Hello").
  Add("es", "Hola").
  Add("fr", "Bonjour")
```

The JSON representation would be:
```json
{
  "def": "en",
  "map": {
    "en": "Hello",
    "es": "Hola",
    "fr": "Bonjour",
  }
}
```
