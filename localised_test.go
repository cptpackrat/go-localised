package localised

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
	"golang.org/x/text/language"
)

func TestLocalised(t *testing.T) {
	for n, f := range map[string]struct {
		assertGood func(t *testing.T, l *Localised[string], expLoc, expVal string, reqLocs ...string)
		assertBad  func(t *testing.T, l *Localised[string], expErr string, reqLocs ...string)
	}{
		"GetFor": {
			assertGood: func(t *testing.T, l *Localised[string], _, expVal string, reqLocs ...string) {
				val, err := l.GetFor(reqLocs...)
				assert.NoError(t, err)
				assert.Equal(t, expVal, val)
			},
			assertBad: func(t *testing.T, l *Localised[string], expErr string, reqLocs ...string) {
				_, err := l.GetFor(reqLocs...)
				assert.EqualError(t, err, expErr)
			},
		},
		"GetLocaleFor": {
			assertGood: func(t *testing.T, l *Localised[string], expLoc, _ string, reqLocs ...string) {
				loc, err := l.GetLocaleFor(reqLocs...)
				assert.NoError(t, err)
				assert.Equal(t, expLoc, loc)
			},
			assertBad: func(t *testing.T, l *Localised[string], expErr string, reqLocs ...string) {
				_, err := l.GetLocaleFor(reqLocs...)
				assert.EqualError(t, err, expErr)
			},
		},
		"MustGetFor": {
			assertGood: func(t *testing.T, l *Localised[string], _, expVal string, reqLocs ...string) {
				assert.Equal(t, expVal, l.MustGetFor(reqLocs...))
			},
			assertBad: func(t *testing.T, l *Localised[string], expErr string, reqLocs ...string) {
				assert.PanicsWithError(t, expErr, func() {
					l.MustGetFor(reqLocs...)
				})
			},
		},
		"MustGetLocaleFor": {
			assertGood: func(t *testing.T, l *Localised[string], expLoc, _ string, reqLocs ...string) {
				assert.Equal(t, expLoc, l.MustGetLocaleFor(reqLocs...))
			},
			assertBad: func(t *testing.T, l *Localised[string], expErr string, reqLocs ...string) {
				assert.PanicsWithError(t, expErr, func() {
					l.MustGetLocaleFor(reqLocs...)
				})
			},
		},
	} {
		t.Run(n, func(t *testing.T) {
			for _, e := range []struct {
				reqLoc, expLoc, expVal string
			}{
				{"en_AU.UTF-8", "en_AU", "g'day"},
				{"en_AU", "en_AU", "g'day"},
				{"en", "en", "hello"},
				{"es", "es", "hola"},
				{"de", "de", "hallo"},
				{"fr", "fr", "bonjour"},
				{"it", "en", "hello"},
			} {
				f.assertGood(t, makeGood(), e.expLoc, e.expVal, e.reqLoc)
				f.assertBad(t, makeBad(), "for locale 'error'; language: tag is not well-formed", e.reqLoc)
			}
			for _, e := range []struct {
				reqLocs        []string
				expLoc, expVal string
			}{
				{[]string{"en", "es", "it"}, "en", "hello"},
				{[]string{"es", "en", "it"}, "es", "hola"},
				{[]string{"it", "en", "es"}, "en", "hello"},
				{[]string{"it", "es", "en"}, "es", "hola"},
			} {
				f.assertGood(t, makeGood(), e.expLoc, e.expVal, e.reqLocs...)
				f.assertBad(t, makeBad(), "for locale 'error'; language: tag is not well-formed", e.reqLocs...)
			}
		})
	}
	for n, f := range map[string]struct {
		assertGood func(t *testing.T, l *Localised[string], expLoc, expVal string, reqTags ...language.Tag)
		assertBad  func(t *testing.T, l *Localised[string], expErr string, reqTags ...language.Tag)
	}{
		"GetForTag": {
			assertGood: func(t *testing.T, l *Localised[string], _, expVal string, reqTags ...language.Tag) {
				val, err := l.GetForTag(reqTags...)
				assert.NoError(t, err)
				assert.Equal(t, expVal, val)
			},
			assertBad: func(t *testing.T, l *Localised[string], expErr string, reqTags ...language.Tag) {
				_, err := l.GetForTag(reqTags...)
				assert.EqualError(t, err, expErr)
			},
		},
		"GetLocaleForTag": {
			assertGood: func(t *testing.T, l *Localised[string], expLoc, _ string, reqTags ...language.Tag) {
				loc, err := l.GetLocaleForTag(reqTags...)
				assert.NoError(t, err)
				assert.Equal(t, expLoc, loc)
			},
			assertBad: func(t *testing.T, l *Localised[string], expErr string, reqTags ...language.Tag) {
				_, err := l.GetLocaleForTag(reqTags...)
				assert.EqualError(t, err, expErr)
			},
		},
		"MustGetForTag": {
			assertGood: func(t *testing.T, l *Localised[string], _, expVal string, reqTags ...language.Tag) {
				assert.Equal(t, expVal, l.MustGetForTag(reqTags...))
			},
			assertBad: func(t *testing.T, l *Localised[string], expErr string, reqTags ...language.Tag) {
				assert.PanicsWithError(t, expErr, func() {
					l.MustGetForTag(reqTags...)
				})
			},
		},
		"MustGetLocaleForTag": {
			assertGood: func(t *testing.T, l *Localised[string], expLoc, _ string, reqTags ...language.Tag) {
				assert.Equal(t, expLoc, l.MustGetLocaleForTag(reqTags...))
			},
			assertBad: func(t *testing.T, l *Localised[string], expErr string, reqTags ...language.Tag) {
				assert.PanicsWithError(t, expErr, func() {
					l.MustGetLocaleForTag(reqTags...)
				})
			},
		},
	} {
		t.Run(n, func(t *testing.T) {
			for _, e := range []struct {
				reqTag         language.Tag
				expLoc, expVal string
			}{
				{language.English, "en", "hello"},
				{language.Spanish, "es", "hola"},
				{language.German, "de", "hallo"},
				{language.French, "fr", "bonjour"},
				{language.Italian, "en", "hello"},
			} {
				f.assertGood(t, makeGood(), e.expLoc, e.expVal, e.reqTag)
				f.assertBad(t, makeBad(), "for locale 'error'; language: tag is not well-formed", e.reqTag)
			}
			for _, e := range []struct {
				reqTags        []language.Tag
				expLoc, expVal string
			}{
				{[]language.Tag{language.English, language.Spanish, language.Italian}, "en", "hello"},
				{[]language.Tag{language.Spanish, language.English, language.Italian}, "es", "hola"},
				{[]language.Tag{language.Italian, language.English, language.Spanish}, "en", "hello"},
				{[]language.Tag{language.Italian, language.Spanish, language.English}, "es", "hola"},
			} {
				f.assertGood(t, makeGood(), e.expLoc, e.expVal, e.reqTags...)
				f.assertBad(t, makeBad(), "for locale 'error'; language: tag is not well-formed", e.reqTags...)
			}
		})
	}
	t.Run("MarshalJSON", func(t *testing.T) {
		var m map[string]any
		b, err := json.Marshal(makeGood())
		assert.NoError(t, err)
		assert.NoError(t, json.Unmarshal(b, &m))
		assert.Equal(t, map[string]any{
			"def": "en",
			"map": map[string]any{
				"en":    "hello",
				"en_AU": "g'day",
				"es":    "hola",
				"de":    "hallo",
				"fr":    "bonjour",
			},
		}, m)
		var l Localised[string]
		_, err = json.Marshal(&l)
		assert.ErrorContains(t, err, "no default locale")
		l.v.Default = "en"
		_, err = json.Marshal(&l)
		assert.ErrorContains(t, err, "no default translation")
	})
	t.Run("UnmarshalJSON", func(t *testing.T) {
		var l Localised[string]
		assert.NoError(t, json.Unmarshal([]byte(`{
			"def": "en",
			"map": {
				"en":    "hello",
				"en_AU": "g'day",
				"es":    "hola",
				"de":    "hallo",
				"fr":    "bonjour"
			}
		}`), &l))
		assert.Equal(t, "en", l.v.Default)
		assert.Equal(t, map[string]string{
			"en":    "hello",
			"en_AU": "g'day",
			"es":    "hola",
			"de":    "hallo",
			"fr":    "bonjour",
		}, l.v.Map)
		assert.ErrorContains(t, json.Unmarshal([]byte(`{
		}`), &l), "no default locale")
		assert.ErrorContains(t, json.Unmarshal([]byte(`{
			"map": {
				"en": "hello"
			}
		}`), &l), "no default locale")
		assert.ErrorContains(t, json.Unmarshal([]byte(`{
			"def": "en"
		}`), &l), "no default translation")
		assert.ErrorContains(t, json.Unmarshal([]byte(`{
			"def": "en",
			"map": {}
		}`), &l), "no default translation")
		assert.ErrorContains(t, json.Unmarshal([]byte(`{
			"def": "en",
			"map": {
				"es": "hola"
			}
		}`), &l), "no default translation")
		assert.ErrorContains(t, json.Unmarshal([]byte(`{
			"def": "en",
			"map": {
				"en":    "hello",
				"error": "kaboom"
			}
		}`), &l), "for locale 'error'; language: tag is not well-formed")
	})
}

func makeGood() *Localised[string] {
	l := New("en", "hello")
	l.Add("en_AU", "g'day")
	l.Add("es", "hola")
	l.Add("de", "hallo")
	l.Add("fr", "bonjour")
	return l
}

func makeBad() *Localised[string] {
	return makeGood().Add("error", "kaboom")
}
