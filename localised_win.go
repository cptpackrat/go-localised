//go:build windows

package localised

import (
	"unsafe"

	"golang.org/x/sys/windows"
)

// Returns the current system locale; may return multiple
// values on platforms that support specifying preferences.
//
// On Windows systems it queries GetUserDefaultLocaleName
// and GetSystemDefaultLocaleName, returning the first
// successful result, or "en" if both fail.
func GetLocale() []string {
	if lib, err := windows.LoadDLL("kernel32"); err == nil {
		buf := make([]uint16, 85)
		ptr := unsafe.Pointer(&buf[0])
		if fn, err := lib.FindProc("GetUserDefaultLocaleName"); err == nil {
			if rc, _, _ := fn.Call(uintptr(ptr), uintptr(len(buf))); rc > 0 {
				return []string{windows.UTF16ToString(buf)}
			}
		}
		if fn, err := lib.FindProc("GetSystemDefaultLocaleName"); err == nil {
			if rc, _, _ := fn.Call(uintptr(ptr), uintptr(len(buf))); rc > 0 {
				return []string{windows.UTF16ToString(buf)}
			}
		}
	}
	return []string{"en"}
}
