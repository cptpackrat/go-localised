//go:build windows

package localised

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestLocalisedWin(t *testing.T) {
	//
	// Compared to the Unix version this is really just a smoke test
	// since we can't manipulate the values returned by GetLocale.
	//
	yay := GetLocale()[0]
	nay := "fr"
	if yay == nay {
		nay = "zh"
	}
	loc := New(nay, "failure").Add(yay, "success")
	t.Run("Get", func(t *testing.T) {
		val, err := loc.Get()
		assert.NoError(t, err)
		assert.Equal(t, "success", val)
	})
	t.Run("GetLocale", func(t *testing.T) {
		val, err := loc.GetLocale()
		assert.NoError(t, err)
		assert.Equal(t, yay, val)
	})
	t.Run("MustGet", func(t *testing.T) {
		assert.Equal(t, "success", loc.MustGet())
	})
	t.Run("MustGetLocale", func(t *testing.T) {
		assert.Equal(t, yay, loc.MustGetLocale())
	})
}
