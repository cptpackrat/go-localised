// Package localised provides a simple utility for storing and
// accessing a piece of data with one or more translations.
package localised

import (
	"encoding/json"
	"fmt"
	"strings"

	"golang.org/x/text/language"
)

// Represents a piece of data with one or more translations;
// intended for self-contained storage in a larger document.
//
// Language selection is based on golang.org/x/text/language,
// which attempts to find the most appropriate match for the
// available translations. If no match is found, the default
// locale's translation is used.
//
// The origin of the default locale depends on the origin of
// the object. When marshalling to/from JSON, the default is
// explicitly marked; when creating a Localised from scratch,
// the default is the first translation added to the object.
//
// Implements a custom json.Unmarshaler to validate that the
// resulting object contains at least one translation.
//
// For the following collection of strings:
//
//	l := New("en", "Hello")
//	l.Add("es", "Hola")
//	l.Add("fr", "Bonjour")
//
// The JSON representation would be:
//
//	{
//		"def": "en",
//		"map": {
//		  "en": "Hello",
//		  "es": "Hola",
//		  "fr": "Bonjour",
//		}
//	}
//
// For instances of Localised that may be accessed from multiple
// threads, it is recommended to call Prepare manually while the
// object is still owned by a single thread. For objects created
// by json.Unmarshal this is done automatically.
type Localised[T any] struct {
	m language.Matcher
	l []string
	v struct {
		Map     map[string]T `json:"map"`
		Default string       `json:"def"`
	}
}

// Returns a new Localised with the given locale as the default translation.
func New[T any](loc string, val T) *Localised[T] {
	l := &Localised[T]{}
	l.Set(loc, val)
	return l
}

// Initialise an existing Localised with the given locale as the default translation.
func (l *Localised[T]) Set(loc string, val T) *Localised[T] {
	*l = Localised[T]{}
	l.v.Default = splitLocale(loc)
	l.v.Map = map[string]T{l.v.Default: val}
	return l
}

// Adds a translation for the given locale.
func (l *Localised[T]) Add(loc string, val T) *Localised[T] {
	l.l = nil
	l.m = nil
	l.v.Map[splitLocale(loc)] = val
	return l
}

// Returns the best match for the current system locale.
func (l *Localised[T]) Get() (T, error) {
	loc, err := l.GetLocale()
	if err != nil {
		var val T
		return val, err
	}
	return l.v.Map[loc], nil
}

// Returns the best match for the given locale.
func (l *Localised[T]) GetFor(locs ...string) (T, error) {
	loc, err := l.GetLocaleFor(locs...)
	if err != nil {
		var val T
		return val, err
	}
	return l.v.Map[loc], nil
}

// Returns the best match for the given language tag.
func (l *Localised[T]) GetForTag(tags ...language.Tag) (T, error) {
	loc, err := l.GetLocaleForTag(tags...)
	if err != nil {
		var val T
		return val, err
	}
	return l.v.Map[loc], nil
}

// Returns the name of the best match for the current system locale.
func (l *Localised[T]) GetLocale() (string, error) {
	return l.GetLocaleFor(GetLocale()...)
}

// Returns the name of the best match for the given locale.
func (l *Localised[T]) GetLocaleFor(locs ...string) (string, error) {
	if err := l.Prepare(); err != nil {
		return "", err
	}
	_, idx := language.MatchStrings(l.m, splitLocales(locs)...)
	return l.l[idx], nil
}

// Returns the name of the best match for the given language tag.
func (l *Localised[T]) GetLocaleForTag(tags ...language.Tag) (string, error) {
	if err := l.Prepare(); err != nil {
		return "", err
	}
	_, idx, _ := l.m.Match(tags...)
	return l.l[idx], nil
}

// Alternate version of Get that panics on error.
func (l *Localised[T]) MustGet() T {
	return l.v.Map[l.MustGetLocale()]
}

// Alternate version of GetFor that panics on error.
func (l *Localised[T]) MustGetFor(locs ...string) T {
	return l.v.Map[l.MustGetLocaleFor(locs...)]
}

// Alternate version of GetForTag that panics on error.
func (l *Localised[T]) MustGetForTag(tags ...language.Tag) T {
	return l.v.Map[l.MustGetLocaleForTag(tags...)]
}

// Alternate version of GetLocale that panics on error.
func (l *Localised[T]) MustGetLocale() string {
	loc, err := l.GetLocale()
	if err != nil {
		panic(err)
	}
	return loc
}

// Alternate version of GetLocaleFor that panics on error.
func (l *Localised[T]) MustGetLocaleFor(locs ...string) string {
	loc, err := l.GetLocaleFor(locs...)
	if err != nil {
		panic(err)
	}
	return loc
}

// Alternate version of GetLocaleForTag that panics on error.
func (l *Localised[T]) MustGetLocaleForTag(tags ...language.Tag) string {
	loc, err := l.GetLocaleForTag(tags...)
	if err != nil {
		panic(err)
	}
	return loc
}

// Prepares a populated object for use.
//
// Called automatically by json.Unmarshal, or on the first access to
// the object if it was populated manually. This method usually does
// not need to be called manually, but may be advisable for manually
// populated objects that will be accessed from multiple threads.
func (l *Localised[T]) Prepare() error {
	if l.m == nil {
		def, err := language.Parse(l.v.Default)
		if err != nil {
			return fmt.Errorf("for locale '%s'; %w", l.v.Default, err)
		}
		locs := []string{l.v.Default}
		tags := []language.Tag{def}
		for loc := range l.v.Map {
			if loc != l.v.Default {
				tag, err := language.Parse(loc)
				if err != nil {
					return fmt.Errorf("for locale '%s'; %w", loc, err)
				}
				locs = append(locs, loc)
				tags = append(tags, tag)
			}
		}
		l.l = locs
		l.m = language.NewMatcher(tags)
	}
	return nil
}

func (l *Localised[T]) MarshalJSON() ([]byte, error) {
	if err := l.validate(); err != nil {
		return nil, err
	}
	return json.Marshal(&l.v)
}

func (l *Localised[T]) UnmarshalJSON(data []byte) error {
	*l = Localised[T]{}
	if err := json.Unmarshal(data, &l.v); err != nil {
		return err
	}
	if err := l.validate(); err != nil {
		return err
	}
	return l.Prepare()
}

func (l *Localised[T]) validate() error {
	if l.v.Default == "" {
		return fmt.Errorf("no default locale")
	}
	if _, ok := l.v.Map[l.v.Default]; !ok {
		return fmt.Errorf("no default translation")
	}
	return nil
}

func splitLocale(loc string) string {
	loc, _, _ = strings.Cut(loc, ".")
	return loc
}

func splitLocales(src []string) []string {
	dst := make([]string, len(src))
	for n, loc := range src {
		dst[n] = splitLocale(loc)
	}
	return dst
}
