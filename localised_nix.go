//go:build unix

package localised

import (
	"os"
	"strings"
)

// Returns the current system locale; may return multiple
// values on platforms that support specifying preferences.
//
// On Unix-Like systems it reads from the environment
// variables LANGUAGE, LC_ALL, LC_MESSAGES, and LANG
// in that order, returning the first non-empty value.
// Returns "en" if all are empty, or if LC_ALL is set
// to "C" or "POSIX".
func GetLocale() []string {
	lc_all, _ := os.LookupEnv("LC_ALL")
	if lc_all != "C" && lc_all != "POSIX" {
		if lang, _ := os.LookupEnv("LANGUAGE"); lang != "" {
			return strings.Split(lang, ":")
		}
		if lc_all != "" {
			return []string{splitLocale(lc_all)}
		}
		if lang, _ := os.LookupEnv("LC_MESSAGES"); lang != "" {
			return []string{splitLocale(lang)}
		}
		if lang, _ := os.LookupEnv("LANG"); lang != "" {
			return []string{splitLocale(lang)}
		}
	}
	return []string{"en"}
}
