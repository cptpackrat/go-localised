//go:build unix

package localised

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestLocalisedNix(t *testing.T) {
	for n, f := range map[string]struct {
		assertGood func(t *testing.T, l *Localised[string], expLoc, expVal string)
		assertBad  func(t *testing.T, l *Localised[string], expErr string)
	}{
		"Get": {
			assertGood: func(t *testing.T, l *Localised[string], _, expVal string) {
				val, err := l.Get()
				assert.NoError(t, err)
				assert.Equal(t, expVal, val)
			},
			assertBad: func(t *testing.T, l *Localised[string], expErr string) {
				_, err := l.Get()
				assert.EqualError(t, err, expErr)
			},
		},
		"GetLocale": {
			assertGood: func(t *testing.T, l *Localised[string], expLoc, _ string) {
				loc, err := l.GetLocale()
				assert.NoError(t, err)
				assert.Equal(t, expLoc, loc)
			},
			assertBad: func(t *testing.T, l *Localised[string], expErr string) {
				_, err := l.GetLocale()
				assert.EqualError(t, err, expErr)
			},
		},
		"MustGet": {
			assertGood: func(t *testing.T, l *Localised[string], _, expVal string) {
				assert.Equal(t, expVal, l.MustGet())
			},
			assertBad: func(t *testing.T, l *Localised[string], expErr string) {
				assert.PanicsWithError(t, expErr, func() {
					l.MustGet()
				})
			},
		},
		"MustGetLocale": {
			assertGood: func(t *testing.T, l *Localised[string], expLoc, _ string) {
				assert.Equal(t, expLoc, l.MustGetLocale())
			},
			assertBad: func(t *testing.T, l *Localised[string], expErr string) {
				assert.PanicsWithError(t, expErr, func() {
					l.MustGetLocale()
				})
			},
		},
	} {
		t.Run(n, func(t *testing.T) {
			for _, e := range []struct {
				envLang, expLoc, expVal string
			}{
				{"en_AU.UTF-8", "en_AU", "g'day"},
				{"en_AU", "en_AU", "g'day"},
				{"en", "en", "hello"},
				{"es", "es", "hola"},
				{"de", "de", "hallo"},
				{"fr", "fr", "bonjour"},
				{"it", "en", "hello"},
			} {
				os.Setenv("LANG", e.envLang)
				os.Setenv("LC_ALL", "")
				os.Setenv("LANGUAGE", "")
				f.assertGood(t, makeGood(), e.expLoc, e.expVal)
				f.assertBad(t, makeBad(), "for locale 'error'; language: tag is not well-formed")
			}
		})
	}
}

func TestGetLocaleNix(t *testing.T) {
	for _, c := range []struct {
		lang, lc_all, lc_msg, language string
		exp                            []string
	}{
		{"", "", "", "", []string{"en"}},
		{"es", "", "", "", []string{"es"}},
		{"es_ES", "", "", "", []string{"es_ES"}},
		{"es_ES.UTF-8", "", "", "", []string{"es_ES"}},
		{"", "es", "", "", []string{"es"}},
		{"", "es_ES", "", "", []string{"es_ES"}},
		{"", "es_ES.UTF-8", "", "", []string{"es_ES"}},
		{"", "", "es", "", []string{"es"}},
		{"", "", "es_ES", "", []string{"es_ES"}},
		{"", "", "es_ES.UTF-8", "", []string{"es_ES"}},
		{"", "", "", "es", []string{"es"}},
		{"", "", "", "es_ES:es", []string{"es_ES", "es"}},
		{"it", "fr", "", "", []string{"fr"}},
		{"it", "", "fr", "", []string{"fr"}},
		{"it", "", "", "es_ES:es", []string{"es_ES", "es"}},
		{"", "fr", "", "es_ES:es", []string{"es_ES", "es"}},
		{"", "", "fr", "es_ES:es", []string{"es_ES", "es"}},
		{"it", "fr", "", "es_ES:es", []string{"es_ES", "es"}},
		{"it", "", "fr", "es_ES:es", []string{"es_ES", "es"}},
		{"it", "de", "fr", "es_ES:es", []string{"es_ES", "es"}},
		{"it", "C", "fr", "es_ES:es", []string{"en"}},
		{"", "C", "", "", []string{"en"}},
	} {
		os.Setenv("LANG", c.lang)
		os.Setenv("LC_ALL", c.lc_all)
		os.Setenv("LC_MESSAGES", c.lc_msg)
		os.Setenv("LANGUAGE", c.language)
		assert.Equal(t, c.exp, GetLocale())
	}
}
